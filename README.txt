CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Current Maintainer: Andrew Podlubnyj <andron13@gmail.com>

A splendid module with Christmas lights decoration that creates longlasting 
atmosphere of X-mas for you and the users of your website.

Merry Christmas!


INSTALLATION
------------

 1. Download and enable the Christmas Lights module.
 2. Go to module configuration page: admin/config/system/christmas_lights.
 3. Enable it. Select start date and end date.
 4. Christmas Lighst will appear at the top of your page. Just fun!
